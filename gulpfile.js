"use strict";

var gulp = require("gulp");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var browserSync = require("browser-sync").create();
var fileinclude = require('gulp-file-include');
var notify = require("gulp-notify");
var sass = require("gulp-sass");
var cleanCSS = require("gulp-clean-css");
var autoprefixer = require("gulp-autoprefixer");

var project = 'ucs-luan';
var assetsPath = ['./']; // Define path dos assets
var viewsPath = ['./']; // Define path das views


gulp.task("serve", ["sass"], function() {
    browserSync.init({
        server: './'
    });

    gulp.watch(assetsPath + "_scss/*.scss", ["sass"]).on('change', browserSync.reload);
    gulp.watch([viewsPath + '_pages/*.html', viewsPath + '_includes/*.html'], ["fileinclude"]).on('change', browserSync.reload);
    gulp.watch([assetsPath + "js/*.js", "!" + assetsPath + "js/*.min.js"], ["js"]);
});

gulp.task('fileinclude', function() {
    gulp.src([viewsPath + '_pages/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }).on("error", notify.onError(function (error) {
            return "Error: " + error.message + '\n' + error.line + ':' + error.index
        })))
        .pipe(gulp.dest('./'));
});


gulp.task("sass", function() {
    return gulp.src(assetsPath + "_scss/{*.scss,_*.scss}")
        .pipe(sass().on("error", notify.onError(function (error) {
            return "Error: " + error.message + '\n' + error.line + ':' + error.index
        })))
        .pipe(autoprefixer())
        .pipe(gulp.dest(assetsPath + "css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(assetsPath + "css"))
        .pipe(browserSync.stream());
});


gulp.task("js", function(){
    return gulp.src([assetsPath + "js/*.js", "!" + assetsPath + "js/*.min.js"])
        .pipe(uglify().on("error", notify.onError(function (error) {
            return "Error: " + error.message + '\n' + error.line + ':' + error.index
        })))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(assetsPath + "js"))
        .pipe(browserSync.stream());
});

// Run default task
gulp.task("default", ["serve"]);